﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ContactListApi.Identity;
using ContactListApi.Jwt;
using ContactListApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace ContactListApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly JwtOptions jwtOptions;
        private readonly UserManager<AppUser> userManager;
        private readonly UsersDbContext usersDbContext;

        public AccountsController(
            UserManager<AppUser> userManager,
            IOptions<JwtOptions> jwtOptions,
            UsersDbContext usersDbContext)
        {
            this.userManager = userManager;
            this.usersDbContext = usersDbContext;
            this.jwtOptions = jwtOptions.Value;
        }

        // POST api/accounts/register
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterCredentials register)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var user = new AppUser
            {
                UserName = register.Email,
                Email = register.Email,
                Name = register.Name,
                Surname = register.Surname
            };

            var result = await userManager.CreateAsync(user, register.Password);

            if (result.Succeeded)
                return Ok();

            return BadRequest();
        }

        // POST api/accounts/login
        [HttpPost("login")]
        public async Task<ActionResult<Tokens>> Login(LoginCredentials login)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var user = await userManager.FindByNameAsync(login.Email);

            if (user == null)
                return BadRequest();

            if (await userManager.CheckPasswordAsync(user, login.Password))
            {
                var refresh = Guid.NewGuid().ToString();
                usersDbContext.RefreshTokens.Add(new RefreshToken
                {
                    Token = refresh,
                    AppUser = user,
                    Expires = DateTime.Now + TimeSpan.FromDays(30)
                });
                usersDbContext.SaveChanges();

                return new Tokens
                {
                    RefreshToken = refresh,
                    Token = GenerateAccessToken(user)
                };
            }

            return BadRequest();
        }

        [HttpPost("refresh")]
        public async Task<ActionResult<Tokens>> Refresh(string refreshToken)
        {
            var oldToken = usersDbContext.RefreshTokens
                .Include(x => x.AppUser)
                .FirstOrDefault(x => x.Token == refreshToken);

            if (oldToken == null && oldToken.Expires <= DateTime.Now)
                return Unauthorized();

            var newRefresh = Guid.NewGuid().ToString();
            usersDbContext.RefreshTokens.Add(new RefreshToken
            {
                Token = newRefresh,
                AppUser = oldToken.AppUser,
                Expires = DateTime.Now + TimeSpan.FromDays(30)
            });
            usersDbContext.SaveChanges();
            var newTokens = new Tokens
            {
                RefreshToken = newRefresh,
                Token = GenerateAccessToken(oldToken.AppUser)
            };

            usersDbContext.RefreshTokens.Remove(oldToken);
            usersDbContext.SaveChanges();

            return newTokens;
        }

        private string GenerateAccessToken(AppUser user)
        {
            var issuedAt = DateTime.Now;

            var claims = new[]
            {
                 new Claim("id", user.Id),
                 new Claim("sub", user.UserName)
            };

            var jwt = new JwtSecurityToken(
                issuer: jwtOptions.Issuer,
                audience: jwtOptions.Audience,
                claims: claims,
                notBefore: issuedAt,
                expires: issuedAt + jwtOptions.ValidFor,
                signingCredentials: jwtOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
    }
}