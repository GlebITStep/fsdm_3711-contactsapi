﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactListApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ContactListApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly ContactsApiDbContext context;

        public ContactsController(ContactsApiDbContext context)
        {
            this.context = context;
        }

        // GET api/contacts
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Contact>> GetContacts()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == "id").Value;
            return context.Contacts.Where(x => x.AppUserId == userId).ToList();
        }

        // GET api/contacts/1
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<Contact> GetContact(int id)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == "id").Value;
            var contact = context.Contacts.FirstOrDefault(x => x.Id == id && x.AppUserId == userId);

            if (contact == null)
                return NotFound();

            return contact;
        }

        // POST api/contacts
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public IActionResult AddContact(Contact contact)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == "id").Value;
            if (ModelState.IsValid)
            {
                try
                {
                    contact.AppUserId = userId;
                    context.Contacts.Add(contact);
                    context.SaveChanges();
                    return Created($"/api/contacts/{contact.Id}", contact);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }
            return BadRequest();
        }

        // POST api/contacts/bulk
        [HttpPost("bulk")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public IActionResult AddContacts(IEnumerable<Contact> contacts)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == "id").Value;
            if (ModelState.IsValid)
            {
                try
                {
                    foreach (var item in contacts)
                    {
                        item.AppUserId = userId;
                    }
                    context.Contacts.AddRange(contacts);
                    context.SaveChanges();
                    return Created($"/api/contacts", contacts);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }
            return BadRequest();
        }

        // PUT api/contacts/1
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult EditContact(int id, Contact contact)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == "id").Value;
            if (ModelState.IsValid)
            {
                try
                {
                    contact.Id = id;
                    contact.AppUserId = userId;
                    context.Contacts.Update(contact);
                    context.SaveChanges();
                    return Ok(contact);
                }
                catch (Exception)
                {
                    return NotFound();
                }
            }
            return BadRequest();
        }

        // DELETE api/contacts/1
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult DeleteContact(int id)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == "id").Value;
            var contact = context.Contacts.FirstOrDefault(x => x.Id == id && x.AppUserId == userId);
            if (contact != null)
            {
                context.Contacts.Remove(contact);
                context.SaveChanges();
                return NoContent();
            }
            return NotFound();
        }
    }
}