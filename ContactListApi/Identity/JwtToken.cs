﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactListApi.Identity
{
    public class Tokens
    {
        public string RefreshToken { get; set; }
        public string Token { get; set; }
    }
}
